package com.ndb.parent.common.exception;

public class NdbException extends RuntimeException {
	
	
	private String errCode;

	
	public NdbException(String errCode) {
		super(errCode);
		this.errCode = errCode;
	}
	
	public String getErrCode() {
		return errCode;
	}
	
}
