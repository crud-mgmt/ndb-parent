package com.ndb.parent.common.model;

import java.io.Serializable;
public class RoleInfo implements Serializable {
    private String roleSeq;

    private String roleName;

    private String roleType;

    private String status;

	public String getRoleSeq() {
		return roleSeq;
	}

	public void setRoleSeq(String roleSeq) {
		this.roleSeq = roleSeq;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    
}
