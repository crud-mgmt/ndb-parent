package com.ndb.parent.common.model;

import java.io.Serializable;
import java.util.Map;

public class GenericReq implements Serializable{
	
	public GenericReq() {
		super();
	}
	public GenericReq(String className, Map<String, Object> fields) {
		super();
		this.className = className;
		this.fields = fields;
	}
	private String className;
	private Map<String, Object> fields;
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Map<String, Object> getFields() {
		return fields;
	}
	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}
	
}
