package com.ndb.parent.common.model;

import java.io.Serializable;

public class Res implements Serializable{
	/**返回*/
	private boolean flag=true;
	
	/**错误码*/
	private String errcode;
	
	/**错误提示信息*/
	private String message;
	
	private UserInfo user;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}
	
	
	
}
