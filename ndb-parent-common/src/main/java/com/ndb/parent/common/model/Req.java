package com.ndb.parent.common.model;

import java.io.Serializable;

public class Req implements Serializable{
	private String channelId;
	
	private String method;
	
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

}
