package com.ndb.parent.common.constant;

public class CommonMsg {
	public static final String DB_EXEC_FAILED="com.ndb.db.execute.failed";
	public static final String DATE_FORMAT_FAILED="com.ndb.date.format.failed";
}
