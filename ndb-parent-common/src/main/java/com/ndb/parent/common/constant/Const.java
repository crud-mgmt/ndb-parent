package com.ndb.parent.common.constant;

public class Const { 
	public static class ChannelType extends ConstTemplate{
		static {
			ConstTemplate.name="channelId";
		}
		public static String MGMT="mgmt";
		public static String MOBILE="mobile";
	}
	
	public static class Status extends ConstTemplate{
		public static String ACTIVE="N";
		public static String LOCK="L";
		public static String DEL="D";
	}
	
	public static class MethodType extends ConstTemplate{
		public static String QUERY="query";
		public static String ACTION="action";
	}
	
	public static final String USER_KEY="user_key";
	
	public static final String NAS_PATH="/data/nas/";
}
