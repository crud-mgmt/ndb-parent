package com.ndb.db.redis.aop;

import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Service;
/**
 * 替代了<aop:aspectj-autoproxy>
 * @author Administrator
 *
 */
@Service
@EnableAspectJAutoProxy
public class EnableAopBean {

}
