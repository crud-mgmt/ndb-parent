package com.ndb.db.redis;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.util.CollectionUtils;

import com.ndb.db.redis.cacheManager.StringCacheManager;
import com.ndb.db.redis.factory.JedisClusterFactoryImpl;
import com.ndb.db.redis.factory.JedisFactory;
import com.ndb.db.redis.factory.JedisFactoryImpl;
import com.ndb.db.redis.model.RedisPoolConfigProperties;

import redis.clients.jedis.JedisPoolConfig;
@Configuration
@Import({RedisPoolConfigProperties.class})
public class RedisPoolConfig{
	@Autowired
	private RedisPoolConfigProperties redisPoolConfigProperties;
	
	
	@Bean
	@ConfigurationProperties(prefix = "spring.redis.pool-config")
	public JedisPoolConfig redisPoolConfig() {
		JedisPoolConfig config=new JedisPoolConfig();
		/*
		config.setMaxTotal(redisPoolConfigProperties.getMaxTotal());
		config.setMaxIdle(redisPoolConfigProperties.getMaxIdle());
		config.setMinIdle(redisPoolConfigProperties.getMinIdle());
		config.setMaxWaitMillis(redisPoolConfigProperties.getMaxWaitMillis());
		config.setTestOnBorrow(redisPoolConfigProperties.isTestOnBorrow());
		config.setTestWhileIdle(redisPoolConfigProperties.isTestWhileIdle());
		config.setTimeBetweenEvictionRunsMillis(redisPoolConfigProperties.getTimeBetweenEvictionRunsMillis());
		config.setMinEvictableIdleTimeMillis(redisPoolConfigProperties.getMinEvictableIdleTimeMillis());
		*/
		return config;
	}
	
	@Bean
	public JedisFactory jedisFactory() {
		List<String> nodes = redisPoolConfigProperties.getNodes();
		JedisFactory factory=null;
		if(CollectionUtils.isEmpty(nodes)) {
			factory=new JedisFactoryImpl(redisPoolConfig(),redisPoolConfigProperties);
		}else {
			factory=new JedisClusterFactoryImpl(redisPoolConfig(), nodes, redisPoolConfigProperties.getPassword(), redisPoolConfigProperties.getTimeout(), redisPoolConfigProperties.getMaxRetries());			
		}
		return factory;
	}
	
	@Bean
	public StringCacheManager strCacheManager() {
		StringCacheManager manager=new StringCacheManager();
		manager.setFactory(jedisFactory());
		List<Cache> caches=new ArrayList<>();
		manager.setCaches(caches);
		return manager;
	}
}
