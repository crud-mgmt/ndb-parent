package com.ndb.db.redis.cacheManager.cache;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.cache.support.SimpleValueWrapper;

import com.ndb.db.redis.factory.JedisFactory;

public class StringCache extends AbstractRedisCache{
	public StringCache(String nameSpace,String keyName,JedisFactory factory,int timeout) {
		init(factory, keyName, timeout);
	}
	
	@Override
	public <T> T get(Object key, Class<T> type) {
		String val = factory.get(prefix(key));
		return (T) get(type, val);
	}

	private <T> Object get(Class<T> type, String val) {
		if(type.equals(String.class)) {
			return val;
		}else if(type.equals(Integer.class)) {
			return Integer.parseInt(val);
		}else if(type.equals(Boolean.class)) {
			return Boolean.valueOf(val);
		}else if(type.equals(Double.class)) {
			return Double.parseDouble(val);
		}else if(type.equals(Float.class)) {
			return Float.valueOf(val);
		}else if(type.equals(Long.class)) {
			return Long.valueOf(val);
		}else if(type.equals(BigDecimal.class)) {
			return new BigDecimal(val);
		}else if(type.equals(Timestamp.class)) {
			return Timestamp.valueOf(val);
		}
		return val;
	}
	
	public List<String> mget(String... keys){
		return factory.mget(keys);
	}
	
	/**
	 * 设置key value,如果key已经存在则返0,nx==> not exist
	 * 
	 * @param key
	 * @param value
	 * @return 成功返回1 如果存在  发生异常 返回 0
	 */
	public Long setnx(String key, String value) {
		return factory.setnx(key, value,timeout);
	}
	
	public String set(String key, String value) {
		return factory.setex(key, value, timeout);
	}
	/**
	 * 通过key获取value值的长度
	 * @param key
	 * @return
	 */
	public Long strlen(String key) {
		return factory.strlen(key);
	}
	
	public Long append(String key, String str) {
		Boolean exists = factory.exists(key);
		if(!exists) {
			set(key,str);
		}else {
			factory.append(key,str);
		}
		return 1L;
	}
	
	public String subStr(String key,int start,int end) {
		if(end>key.length()) {
			end=-1;
		}
		return factory.getrange(key, start, end);
	}
	/**
	 * 字符串替换
	 * @param key
	 * @param str
	 * @param offset
	 * @return
	 */
	public String strReplace(String key,String str,int offset) {
		if(exists(key)) {
			factory.setrange(key, str, offset);
			return get(key,String.class);
		}
		return null;
	}
	
	/**
	 * 对值进行+1
	 * @param key
	 * @return
	 */
	public Long incr(String key) {
		Long count = factory.incr(key);
		if(!exists(key)) {
			factory.expire(key, timeout);
		}
		return count;
	}
	
	/**
	 * 对值进行+val
	 * @param key
	 * @return
	 */
	public Long incrBy(String key,int val) {
		Long count = factory.incrBy(key, Long.valueOf(val));
		if(!exists(key)) {
			factory.expire(key, timeout);
		}
		return count;
	}
	
	/**
	 * 对值进行-1
	 * @param key
	 * @return
	 */
	public Long decr(String key) {
		Long count = factory.decr(key);
		if(!exists(key)) {
			factory.expire(key, timeout);
		}
		return count;
	}
	
	/**
	 * 对值进行-val
	 * @param key
	 * @return
	 */
	public Long decrBy(String key,int val) {
		Long count = factory.decrBy(key, Long.valueOf(val));
		if(!exists(key)) {
			factory.expire(key, timeout);
		}
		return count;
	}

	@Override
	public ValueWrapper get(Object key) {
		String value = factory.get(prefix(key));
		return new SimpleValueWrapper(value);
	}
	
}
