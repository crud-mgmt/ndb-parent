package com.ndb.db.redis.convert;

public interface Converter {
	Object convert(String key);
}
