package com.ndb.db.redis.cacheManager.cache;

import java.util.Set;
import java.util.concurrent.Callable;

import org.springframework.cache.Cache;

import com.ndb.db.redis.factory.JedisFactory;

public abstract class AbstractRedisCache implements Cache{
	protected JedisFactory factory;
	/**缓存前缀名称*/
	protected String name;
	/**实际存入redis的缓存名*/
	protected String cacheKey;
	protected int timeout;
	
	public void init(JedisFactory factory, String name, int timeout) {
		this.factory = factory;
		this.name = name;
		this.timeout = timeout;
	}
	/**
	 * 拼装redis缓存的key
	 * @param key
	 * @return
	 */
	protected String prefix(Object key) {
		StringBuilder sb = new StringBuilder();
		sb.append(name).append("@").append(key);
		return sb.toString();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Object getNativeCache() {
		return factory;
	}

	@Override
	public <T> T get(Object key, Callable<T> valueLoader) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void put(Object key, Object value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void evict(Object key) {
		factory.del(prefix(key));
	}

	@Override
	public void clear() {
		Set<String> keys = factory.keys(prefix("*"));
		factory.del(keys.toArray(new String[keys.size()]));
	}
	
	public boolean exists(String key) {
		return factory.exists(key);
	}
	
	public boolean delKey(String... keys) {
		Long count = factory.del(keys);
		return count>0;
	}
}
