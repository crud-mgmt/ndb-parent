package com.ndb.db.redis.cacheManager;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;

import com.ndb.db.redis.constants.RedisExprieEnum;
import com.ndb.db.redis.convert.Converter;
import com.ndb.db.redis.convert.RedisExpriedConvert;

public abstract class AbstractRedisCacheManager extends AbstractCacheManager{
	/**过期时间*/
	protected Integer timeout;
	
	protected Converter converter;
	
	private Collection<? extends Cache> caches = Collections.emptySet();
	
	public void setCaches(Collection<? extends Cache> caches) {
		this.caches = caches;
	}
	
	public Integer getTimeout() {
		return timeout;
	}
	@Override
	protected Collection<? extends Cache> loadCaches() {
		return this.caches;
	}
	
	public abstract <T> T getCache(String name,String exprieType);
	
	@Override
	public void afterPropertiesSet(){
		super.afterPropertiesSet();
		RedisExpriedConvert redisConverter=new RedisExpriedConvert();
		Map<String, Integer> exprieMapping = initRedisExprieMapping();
		redisConverter.setExprieMapping(exprieMapping);
		converter=redisConverter;
	}

	private Map<String, Integer> initRedisExprieMapping() {
		Map<String, Integer> exprieMapping=new HashMap<String, Integer>();
		RedisExprieEnum[] values = RedisExprieEnum.values();
		for (RedisExprieEnum item : values) {
			String key = item.getName();
			int value = item.getExprie();
			exprieMapping.put(key, value);
		}
		return exprieMapping;
	}

}
