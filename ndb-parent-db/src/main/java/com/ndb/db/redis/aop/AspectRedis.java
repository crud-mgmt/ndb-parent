package com.ndb.db.redis.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class AspectRedis {

	
	@Pointcut("execution(public * com.ndb.db.redis.cacheManager.cache.StringCache.*(..))")
	public void pc1() {};
	
	@Around(value = "pc1()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		System.out.println("=======前置通知==========");
		Object[] args = point.getArgs();
		for (Object object : args) {
			if(object instanceof String) {
				String key=(String) object;
			}
		}
		Object result = point.proceed();
		System.out.println("=======后置通知==========");
		System.out.println("-------result:"+result);
		return result;
		
	}

}
