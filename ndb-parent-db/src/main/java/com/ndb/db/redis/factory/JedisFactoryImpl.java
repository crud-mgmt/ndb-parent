package com.ndb.db.redis.factory;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.util.StringUtils;

import com.ndb.db.redis.model.RedisPoolConfigProperties;

import redis.clients.jedis.BinaryClient.LIST_POSITION;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.Pipeline;
import redis.clients.util.Pool;

public class JedisFactoryImpl extends AbstractJedisFactory {

	private Pool<Jedis> pool;
	public JedisFactoryImpl(GenericObjectPoolConfig<?> poolConfig,RedisPoolConfigProperties redisPoolConfigProperties) {
		this(poolConfig, redisPoolConfigProperties.getUrl(), redisPoolConfigProperties.getPassword(), redisPoolConfigProperties.getTimeout(), 0);
	}
	public JedisFactoryImpl(GenericObjectPoolConfig<?> poolConfig,String url,String password,int timeout,int database) {
		if(StringUtils.isEmpty(password)) {
			password=null;
		}
		int index = url.indexOf('@');
		if(index==-1){
			HostAndPort address = HostAndPort.parseString(url);
			pool=new JedisPool(poolConfig, address.getHost(), address.getPort(), timeout, password, database);
		}else {
			//针对哨兵模式
			//mymaster@127.0.0.1:6379,192.168.231.87:6379
			String masterName=url.substring(0,index);
			String[] urls=url.substring(index+1).split(",");
			Set<String> ipSet=new HashSet<>();
			for (String host : urls) {
				ipSet.add(host);
			}
			pool=new JedisSentinelPool(masterName, ipSet, poolConfig, timeout,password,database);
		}
	}
	
	@Override
	public void destroy() throws Exception {
		pool.close();
	}
	
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#getJedis()
	 */
	@Override
	public Jedis getJedis() {
		return pool.getResource();
	}
	
	/* (non-Javadoc)
	 * @see @see com.ndb.db.redis.JedisFactory#returnResource(redis.clients.jedis.Jedis)
	 */
	@Override
	public void returnResource(Jedis jedis) {
		if (jedis != null) {
			jedis.close();
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#get(java.lang.String)
	 */
	@Override
	public String get(String key) {
		Jedis jedis = null;
		
		String value = null;
		try {
			jedis = getJedis();
			value = jedis.get(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return value;
	}


	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#set(java.lang.String, java.lang.String)
	 */
	@Override
	public String set(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.set(key, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return "0";
		} finally {
			returnResource(jedis);
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#setSerializer(byte[], byte[])
	 */
	@Override
	public String setSerializer(byte[] keyBytes, byte[] valueBytes) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.set(keyBytes, valueBytes);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return "0";
		} finally {
			returnResource(jedis);
		}
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#getSerializer(byte[])
	 */
	@Override
	public byte[] getSerializer(byte[] key) {
		Jedis jedis = null;
		byte[] value = null;
		try {
			jedis = getJedis();
			value = jedis.get(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return value;
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#del(java.lang.String)
	 */
	@Override
	public Long del(String... keys) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.del(keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return 0L;
		} finally {
			returnResource(jedis);
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#append(java.lang.String, java.lang.String)
	 */
	@Override
	public Long append(String key, String str) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.append(key, str);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return 0L;
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#exists(java.lang.String)
	 */
	@Override
	public Boolean exists(String key) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.exists(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return false;
		} finally {
			returnResource(jedis);
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#setnx(java.lang.String, java.lang.String)
	 */
	@Override
	public Long setnx(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.setnx(key, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return 0L;
		} finally {
			returnResource(jedis);
		}
	}
	
	@Override
	public Long setnx(String key, String value, int timeout) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			String result = jedis.set(key, value,"NX","EX", timeout);
			return "0".equals(result)?0L:1L;
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return 0L;
		} finally {
			returnResource(jedis);
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#setex(java.lang.String, java.lang.String, int)
	 */
	@Override
	public String setex(String key, String value, int seconds) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.setex(key, seconds, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#setrange(java.lang.String, java.lang.String, int)
	 */
	@Override
	public Long setrange(String key, String str, int offset) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			return jedis.setrange(key, offset, str);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
			return 0L;
		} finally {
			returnResource(jedis);
		}
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#mget(java.lang.String)
	 */
	@Override
	public List<String> mget(String... keys) {
		Jedis jedis = null;
		List<String> values = null;
		try {
			jedis = getJedis();
			values = jedis.mget(keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return values;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#mset(java.lang.String)
	 */
	@Override
	public String mset(String... keysvalues) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.mset(keysvalues);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#mdel(java.util.List)
	 */
	@Override
	public boolean mdel(List<String> keys) {
		Jedis jedis = null;
		boolean flag = false;
		try {
			jedis = getJedis();
			Pipeline pipe = jedis.pipelined();//获取jedis对象的pipeline对象
			for(String key:keys){
				pipe.del(key); //将多个key放入pipe删除指令
			}
			pipe.sync(); //执行命令，完全此时pipeline对象的远程调 
			flag = true;
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return flag;
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#msetnx(java.lang.String)
	 */
	@Override
	public Long msetnx(String... keysvalues) {
		Jedis jedis = null;
		Long res = 0L;
		try {
			jedis = getJedis();
			res = jedis.msetnx(keysvalues);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#getset(java.lang.String, java.lang.String)
	 */
	@Override
	public String getset(String key, String value) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.getSet(key, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#getrange(java.lang.String, int, int)
	 */
	@Override
	public String getrange(String key, int startOffset, int endOffset) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.getrange(key, startOffset, endOffset);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#incr(java.lang.String)
	 */
	@Override
	public Long incr(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.incr(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#incrBy(java.lang.String, java.lang.Long)
	 */
	@Override
	public Long incrBy(String key, Long integer) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.incrBy(key, integer);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#decr(java.lang.String)
	 */
	@Override
	public Long decr(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.decr(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#decrBy(java.lang.String, java.lang.Long)
	 */
	@Override
	public Long decrBy(String key, Long integer) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.decrBy(key, integer);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#serlen(java.lang.String)
	 */
	@Override
	public Long strlen(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.strlen(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hset(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Long hset(String key, String field, String value) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.hset(key, field, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hsetnx(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Long hsetnx(String key, String field, String value) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.hsetnx(key, field, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hmset(java.lang.String, java.util.Map)
	 */
	@Override
	public String hmset(String key, Map<String, String> hash) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.hmset(key, hash);			
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hget(java.lang.String, java.lang.String)
	 */
	@Override
	public String hget(String key, String field) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.hget(key, field);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hmget(java.lang.String, java.lang.String)
	 */
	@Override
	public List<String> hmget(String key, String... fields) {
		Jedis jedis = null;
		List<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.hmget(key, fields);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hincrby(java.lang.String, java.lang.String, java.lang.Long)
	 */
	@Override
	public Long hincrby(String key, String field, Long value) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.hincrBy(key, field, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hexists(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean hexists(String key, String field) {
		Jedis jedis = null;
		Boolean res = false;
		try {
			jedis = getJedis();
			res = jedis.hexists(key, field);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hlen(java.lang.String)
	 */
	@Override
	public Long hlen(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.hlen(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;

	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hgetAll(java.lang.String)
	 */
	@Override
	public Map<String,String> hgetAll(String key) {
		Jedis jedis = null;
		Map<String,String> res = null;
		try {
			jedis = getJedis();
			res = jedis.hgetAll(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;

	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hdel(java.lang.String, java.lang.String)
	 */
	@Override
	public Long hdel(String key, String... fields) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.hdel(key, fields);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
		/* (non-Javadoc)
		 * @see com.ndb.db.redis.JedisFactory#zrange(java.lang.String, java.lang.Long, java.lang.Long)
		 */
		@Override
		public Set<String> zrange(String key, Long start, Long end) {
			Jedis jedis = null;
			Set<String> res = null;
			try {
				jedis = getJedis();
				res = jedis.zrange(key, start, end);
			} catch (Exception e) {
				returnResource(jedis);
				e.printStackTrace();
			} finally {
				returnResource(jedis);
			}
			return res;
		}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hkeys(java.lang.String)
	 */
	@Override
	public Set<String> hkeys(String key) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.hkeys(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hvals(java.lang.String)
	 */
	@Override
	public List<String> hvals(String key) {
		Jedis jedis = null;
		List<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.hvals(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hgetall(java.lang.String)
	 */
	@Override
	public Map<String, String> hgetall(String key) {
		Jedis jedis = null;
		Map<String, String> res = null;
		try {
			jedis = getJedis();
			res = jedis.hgetAll(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lpush(java.lang.String, java.lang.String)
	 */
	@Override
	public Long lpush(String key, String... strs) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.lpush(key, strs);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#rpush(java.lang.String, java.lang.String)
	 */
	@Override
	public Long rpush(String key, String... strs) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.rpush(key, strs);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#linsert(java.lang.String, redis.clients.jedis.BinaryClient.LIST_POSITION, java.lang.String, java.lang.String)
	 */
	@Override
	public Long linsert(String key, LIST_POSITION where, String pivot, String value) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.linsert(key, where, pivot, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lset(java.lang.String, java.lang.Long, java.lang.String)
	 */
	@Override
	public String lset(String key, Long index, String value) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.lset(key, index, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lrem(java.lang.String, long, java.lang.String)
	 */
	@Override
	public Long lrem(String key, long count, String value) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.lrem(key, count, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#ltrim(java.lang.String, long, long)
	 */
	@Override
	public String ltrim(String key, long start, long end) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.ltrim(key, start, end);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lpop(java.lang.String)
	 */
	@Override
	public String lpop(String key) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.lpop(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#rpop(java.lang.String)
	 */
	@Override
	public String rpop(String key) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.rpop(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#rpoplpush(java.lang.String, java.lang.String)
	 */
	@Override
	public String rpoplpush(String srckey, String dstkey) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.rpoplpush(srckey, dstkey);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lindex(java.lang.String, long)
	 */
	@Override
	public String lindex(String key, long index) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.lindex(key, index);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#llen(java.lang.String)
	 */
	@Override
	public Long llen(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.llen(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#lrange(java.lang.String, long, long)
	 */
	@Override
	public List<String> lrange(String key, long start, long end) {
		Jedis jedis = null;
		List<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.lrange(key, start, end);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sadd(java.lang.String, java.lang.String)
	 */
	@Override
	public Long sadd(String key, String... members) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.sadd(key, members);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
	
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#expire(java.lang.String, int)
	 */
	@Override
	public void expire(String key, int times) {
		Jedis jedis = null;
		 
		try {
			jedis = getJedis();
			jedis.expire(key, times);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#srem(java.lang.String, java.lang.String)
	 */
	@Override
	public Long srem(String key, String... members) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.srem(key, members);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#spop(java.lang.String)
	 */
	@Override
	public String spop(String key) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.spop(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sdiff(java.lang.String)
	 */
	@Override
	public Set<String> sdiff(String... keys) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.sdiff(keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sdiffstore(java.lang.String, java.lang.String)
	 */
	@Override
	public Long sdiffstore(String dstkey, String... keys) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.sdiffstore(dstkey, keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sinter(java.lang.String)
	 */
	@Override
	public Set<String> sinter(String... keys) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.sinter(keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sinterstore(java.lang.String, java.lang.String)
	 */
	@Override
	public Long sinterstore(String dstkey, String... keys) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.sinterstore(dstkey, keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sunion(java.lang.String)
	 */
	@Override
	public Set<String> sunion(String... keys) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.sunion(keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sunionstore(java.lang.String, java.lang.String)
	 */
	@Override
	public Long sunionstore(String dstkey, String... keys) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.sunionstore(dstkey, keys);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#smove(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Long smove(String srckey, String dstkey, String member) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.smove(srckey, dstkey, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#scard(java.lang.String)
	 */
	@Override
	public Long scard(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.scard(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#sismember(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean sismember(String key, String member) {
		Jedis jedis = null;
		Boolean res = null;
		try {
			jedis = getJedis();
			res = jedis.sismember(key, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#srandmember(java.lang.String)
	 */
	@Override
	public String srandmember(String key) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.srandmember(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#smembers(java.lang.String)
	 */
	@Override
	public Set<String> smembers(String key) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.smembers(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zadd(java.lang.String, java.util.Map)
	 */
	@Override
	public Long zadd(String key, Map<String,Double> scoreMembers) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zadd(key, scoreMembers);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zadd(java.lang.String, double, java.lang.String)
	 */
	@Override
	public Long zadd(String key, double score, String member) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zadd(key, score, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrem(java.lang.String, java.lang.String)
	 */
	@Override
	public Long zrem(String key, String... members) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zrem(key, members);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zincrby(java.lang.String, double, java.lang.String)
	 */
	@Override
	public Double zincrby(String key, double score, String member) {
		Jedis jedis = null;
		Double res = null;
		try {
			jedis = getJedis();
			res = jedis.zincrby(key, score, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#hincrBy(java.lang.String, java.lang.String, long)
	 */
	@Override
	public long hincrBy(String key, String field, long value) {
		Jedis jedis = null;
		long res = 0l;
		try {
			jedis = getJedis();
			res = jedis.hincrBy(key, field, value);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrank(java.lang.String, java.lang.String)
	 */
	@Override
	public Long zrank(String key, String member) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zrank(key, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrevrank(java.lang.String, java.lang.String)
	 */
	@Override
	public Long zrevrank(String key, String member) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zrevrank(key, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrevrange(java.lang.String, long, long)
	 */
	@Override
	public Set<String> zrevrange(String key, long start, long end) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.zrevrange(key, start, end);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrangebyscore(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Set<String> zrangebyscore(String key, String max, String min) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.zrevrangeByScore(key, max, min);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zrangeByScore(java.lang.String, double, double)
	 */
	@Override
	public Set<String> zrangeByScore(String key, double max, double min) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.zrevrangeByScore(key, max, min);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zcount(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Long zcount(String key, String min, String max) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zcount(key, min, max);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zcard(java.lang.String)
	 */
	@Override
	public Long zcard(String key) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zcard(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zscore(java.lang.String, java.lang.String)
	 */
	@Override
	public Double zscore(String key, String member) {
		Jedis jedis = null;
		Double res = null;
		try {
			jedis = getJedis();
			res = jedis.zscore(key, member);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zremrangeByRank(java.lang.String, long, long)
	 */
	@Override
	public Long zremrangeByRank(String key, long start, long end) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zremrangeByRank(key, start, end);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#zremrangeByScore(java.lang.String, double, double)
	 */
	@Override
	public Long zremrangeByScore(String key, double start, double end) {
		Jedis jedis = null;
		Long res = null;
		try {
			jedis = getJedis();
			res = jedis.zremrangeByScore(key, start, end);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#keys(java.lang.String)
	 */
	@Override
	public Set<String> keys(String pattern) {
		Jedis jedis = null;
		Set<String> res = null;
		try {
			jedis = getJedis();
			res = jedis.keys(pattern);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see com.ndb.db.redis.JedisFactory#type(java.lang.String)
	 */
	@Override
	public String type(String key) {
		Jedis jedis = null;
		String res = null;
		try {
			jedis = getJedis();
			res = jedis.type(key);
		} catch (Exception e) {
			returnResource(jedis);
			e.printStackTrace();
		} finally {
			returnResource(jedis);
		}
		return res;
	}
}
