package com.ndb.db.redis.constants;

public enum RedisExprieEnum {
	DEFAULT("default",60),
	HOUR("hour",3600),
	DAY("day",86400),
	WEEK("week",604800),
	MONTH("month",2592000),
	YEAR("year",31536000),
	PERSIST("persist",-1),
	TOKEN_CACHE("tokenCache",120),//token缓存时间
	SUBJECT_CACHE("subjectCache",900);//会话缓存时间
	private String name;
	private int exprie;
	
	private RedisExprieEnum(String name, int exprie) {
		this.name = name;
		this.exprie = exprie;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getExprie() {
		return exprie;
	}
	public void setExprie(int exprie) {
		this.exprie = exprie;
	}
	
	
}
