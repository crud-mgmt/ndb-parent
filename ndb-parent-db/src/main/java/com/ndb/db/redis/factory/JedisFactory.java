package com.ndb.db.redis.factory;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.BinaryClient.LIST_POSITION;

public interface JedisFactory extends DisposableBean{

	/**
	 * 获取连接池jedis对象
	 * @return jedis对象
	 */
	Object getJedis();
	
	/**
	 * 返还到连接池
	 * @param jedis
	 */
	void returnResource(Jedis jedis);
	
	/**
	 * 通过key获取储存在redis中的value 并释放连
	 * 
	 * @param key
	 * @return 成功返回value 失败返回null
	 */
	String get(String key);

	/**
	 * 向redis存入key和value,并释放连接资 如果key已经存在 则覆
	 * 
	 * @param key
	 * @param value
	 * @return 成功 返回OK 失败返回 0
	 */
	String set(String key, String value);

	/**
	 * 向redis存入序列化的key和value,并释放连接资 如果key已经存在 则覆
	 * 
	 * @param key
	 * @param value
	 * @return 成功 返回OK 失败返回 0
	 */
	String setSerializer(byte[] keyBytes, byte[] valueBytes);

	/**
	 * 通过序列化key获取储存在redis中的序列化value 并释放连
	 * 
	 * @param key
	 * @return 成功返回value 失败返回null
	 */
	byte[] getSerializer(byte[] key);

	/**
	 * 删除指定的key,也可以传入一个包含key的数
	 * 
	 * @param keys
	 *            个key 也可以使 string 数组
	 * @return 返回删除成功的个
	 */
	Long del(String... keys);

	/**
	 * 通过key向指定的value值追加?
	 * 
	 * @param key
	 * @param str
	 * @return 成功返回 添加后value的长 失败 返回 添加 value 的长 异常返回0L
	 */
	Long append(String key, String str);

	/**
	 * 判断key是否存在
	 * 
	 * @param key
	 * @return true OR false
	 */
	Boolean exists(String key);

	/**
	 * 设置key value,如果key已经存在则返0,nx==> not exist
	 * 
	 * @param key
	 * @param value
	 * @return 成功返回1 如果存在  发生异常 返回 0
	 */
	Long setnx(String key, String value);
	/**
	 * 设置key value,如果key已经存在则返0,nx==> not exist
	 * 
	 * @param key
	 * @param value
	 * @param timeout超时时间
	 * @return 成功返回1 如果存在  发生异常 返回 0
	 */
	Long setnx(String key, String value,int timeout);

	/**
	 * 设置key value并制定这个键值的有效
	 * 
	 * @param key
	 * @param value
	 * @param seconds
	 *            单位：秒
	 * @return 成功返回OK 失败和异常返回null
	 */
	String setex(String key, String value, int seconds);

	/**
	 * 通过key 和offset 从指定的位置始将原先value替换 下标0,offset表示从offset下标始替
	 * 如果替换的字符串长度过小则会这样 example: value : bigsea@zto.cn str : abc 从下7始替 则结果为
	 * RES : bigsea.abc.cn
	 * 
	 * @param key
	 * @param str
	 * @param offset
	 *            下标位置
	 * @return 返回替换 value 的长
	 */
	Long setrange(String key, String str, int offset);

	/**
	 * 通过批量的key获取批量的value
	 * 
	 * @param keys
	 *            string数组 也可以是个key
	 * @return 成功返回value的集, 失败返回null的集 ,异常返回
	 */
	List<String> mget(String... keys);

	/**
	 * 批量的设置key:value,可以 example: obj.mset(new
	 * String[]{"key2","value1","key2","value2"})
	 * 
	 * @param keysvalues
	 * @return 成功返回OK 失败 异常 返回 null
	 * 
	 */
	String mset(String... keysvalues);

	/**
	 * 删除多个字符串key 并释放连
	 * 
	 * @param key*
	 * @return 成功返回value 失败返回null
	 */
	boolean mdel(List<String> keys);

	/**
	 * 批量的设置key:value,可以,如果key已经存在则会失败,操作会回 example: obj.msetnx(new
	 * String[]{"key2","value1","key2","value2"})
	 * 
	 * @param keysvalues
	 * @return 成功返回1 失败返回0
	 */
	Long msetnx(String... keysvalues);

	/**
	 * 设置key的?,并返回一个旧
	 * 
	 * @param key
	 * @param value
	 * @return 旧? 如果key不存 则返回null
	 */
	String getset(String key, String value);

	/**
	 * 通过下标 和key 获取指定下标位置 value
	 * 
	 * @param key
	 * @param startOffset  始位 0  负数表示从右边开始截
	 * @param endOffset 结束位置，-1最后一位，-2倒数第二位
	 * @return 如果没有返回null
	 */
	String getrange(String key, int startOffset, int endOffset);

	/**
	 * 通过key 对value进行加?+1操作,当value不是int类型时会返回错误,当key不存在是则value1
	 * 
	 * @param key
	 * @return 加�后的结
	 */
	Long incr(String key);

	/**
	 * 通过key给指定的value加?,如果key不存,则这是value为该
	 * 
	 * @param key
	 * @param integer
	 * @return
	 */
	Long incrBy(String key, Long integer);

	/**
	 * 对key的�做减减操作,如果key不存,则设置key-1
	 * 
	 * @param key
	 * @return
	 */
	Long decr(String key);

	/**
	 * 减去指定的?
	 * 
	 * @param key
	 * @param integer
	 * @return
	 */
	Long decrBy(String key, Long integer);

	/**
	 * 通过key获取value值的长度
	 * 
	 * @param key
	 * @return 失败返回null
	 */
	Long strlen(String key);

	/**
	 * 通过key给field设置指定的?,如果key不存,则先创建
	 * 
	 * @param key
	 * @param field
	 *            字段
	 * @param value
	 * @return 如果存在返回0 异常返回null
	 */
	Long hset(String key, String field, String value);

	/**
	 * 通过key给field设置指定的?,如果key不存在则先创,如果field已经存在,返回0
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	Long hsetnx(String key, String field, String value);

	/**
	 * 通过key同时设置 hash的多个field
	 * 
	 * @param key
	 * @param hash
	 * @return 返回OK 异常返回null
	 */
	String hmset(String key, Map<String, String> hash);

	/**
	 * 通过key  field 获取指定 value
	 * 
	 * @param key
	 * @param field
	 * @return 没有返回null
	 */
	String hget(String key, String field);

	/**
	 * 通过key  fields 获取指定的value 如果没有对应的value则返回null
	 * 
	 * @param key
	 * @param fields
	 *            可以 个String 也可以是 String数组
	 * @return
	 */
	List<String> hmget(String key, String... fields);

	/**
	 * 通过key给指定的field的value加上给定的?
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	Long hincrby(String key, String field, Long value);

	/**
	 * 通过key和field判断是否有指定的value存在
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	Boolean hexists(String key, String field);

	/**
	 * 通过key返回field的数
	 * 
	 * @param key
	 * @return
	 */
	Long hlen(String key);

	Map<String, String> hgetAll(String key);

	/**
	 * 通过key 删除指定 field
	 * 
	 * @param key
	 * @param fields
	 *            可以  field 也可以是 个数
	 * @return
	 */
	Long hdel(String key, String... fields);

	Set<String> zrange(String key, Long start, Long end);

	/**
	 * 通过key返回有的field
	 * 
	 * @param key
	 * @return
	 */
	Set<String> hkeys(String key);

	/**
	 * 通过key返回有和key有关的value
	 * 
	 * @param key
	 * @return
	 */
	List<String> hvals(String key);

	/**
	 * 通过key获取有的field和value
	 * 
	 * @param key
	 * @return
	 */
	Map<String, String> hgetall(String key);

	/**
	 * <p>
	 * 通过key向list头部添加字符
	 * </p>
	 * 
	 * @param key
	 * @param strs
	 *            可以使一个string 也可以使string数组
	 * @return 返回list的value个数
	 */
	Long lpush(String key, String... strs);

	/**
	 * <p>
	 * 通过key向list尾部添加字符
	 * </p>
	 * 
	 * @param key
	 * @param strs
	 *            可以使一个string 也可以使string数组
	 * @return 返回list的value个数
	 */
	Long rpush(String key, String... strs);

	/**
	 * <p>
	 * 通过key在list指定的位置之前或者之 添加字符串元
	 * </p>
	 * 
	 * @param key
	 * @param where
	 *            LIST_POSITION枚举类型
	 * @param pivot
	 *            list里面的value
	 * @param value
	 *            添加的value
	 * @return
	 */
	Long linsert(String key, LIST_POSITION where, String pivot, String value);

	/**
	 * <p>
	 * 通过key设置list指定下标位置的value
	 * </p>
	 * <p>
	 * 如果下标超过list里面value的个数则报错
	 * </p>
	 * 
	 * @param key
	 * @param index
	 *            0
	 * @param value
	 * @return 成功返回OK
	 */
	String lset(String key, Long index, String value);

	/**
	 * <p>
	 * 通过key从对应的list中删除指定的count  value相同的元
	 * </p>
	 * 
	 * @param key
	 * @param count
	 *            当count0时删除全
	 * @param value 要删除的元素
	 * @return 返回被删除的个数
	 */
	Long lrem(String key, long count, String value);

	/**
	 * <p>
	 * 通过key保留list中从strat下标始到end下标结束的value
	 * </p>
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return 成功返回OK
	 */
	String ltrim(String key, long start, long end);

	/**
	 * <p>
	 * 通过key从list的头部删除一个value,并返回该value
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	String lpop(String key);

	/**
	 * <p>
	 * 通过key从list尾部删除个value,并返回该元素
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	String rpop(String key);

	/**
	 * <p>
	 * 通过key从一个list的尾部删除一个value并添加到另一个list的头,并返回该value
	 * </p>
	 * <p>
	 * 如果第一个list为空或�不存在则返回null
	 * </p>
	 * 
	 * @param srckey
	 * @param dstkey
	 * @return
	 */
	String rpoplpush(String srckey, String dstkey);

	/**
	 * <p>
	 * 通过key获取list中指定下标位置的value
	 * </p>
	 * 
	 * @param key
	 * @param index
	 * @return 如果没有返回null
	 */
	String lindex(String key, long index);

	/**
	 * <p>
	 * 通过key返回list的长
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	Long llen(String key);

	/**
	 * <p>
	 * 通过key获取list指定下标位置的value
	 * </p>
	 * <p>
	 * 如果start  0 end  -1 则返回全部的list中的value
	 * </p>
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	List<String> lrange(String key, long start, long end);

	/**
	 * <p>
	 * 通过key向指定的set中添加value
	 * </p>
	 * 
	 * @param key
	 * @param members
	 *            可以是一个String 也可以是个String数组
	 * @return 添加成功的个
	 */
	Long sadd(String key, String... members);
	
	/**
	 * 设置key的过期时间
	 * @param key
	 * @param times 单位秒
	 */
	void expire(String key, int times);

	/**
	 * <p>
	 * 通过key删除set中对应的value
	 * </p>
	 * 
	 * @param key
	 * @param members
	 *            可以是一个String 也可以是个String数组
	 * @return 删除的个
	 */
	Long srem(String key, String... members);

	/**
	 * <p>
	 * 通过key随机删除个set中的value并返回该
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	String spop(String key);

	/**
	 * <p>
	 * 通过key获取set中的差集
	 * </p>
	 * <p>
	 * 以第个set为标
	 * </p>
	 * 
	 * @param keys
	 *            可以使一个string 则返回set中所有的value 也可以是string数组
	 * @return
	 */
	Set<String> sdiff(String... keys);

	/**
	 * <p>
	 * 通过key获取set中的差集并存入到另一个key
	 * </p>
	 * <p>
	 * 以第个set为标
	 * </p>
	 * 
	 * @param dstkey
	 *            差集存入的key
	 * @param keys
	 *            可以使一个string 则返回set中所有的value 也可以是string数组
	 * @return
	 */
	Long sdiffstore(String dstkey, String... keys);

	/**
	 * <p>
	 * 通过key获取指定set中的交集
	 * </p>
	 * 
	 * @param keys
	 *            可以使一个string 也可以是个string数组
	 * @return
	 */
	Set<String> sinter(String... keys);

	/**
	 * <p>
	 * 通过key获取指定set中的交集 并将结果存入新的set
	 * </p>
	 * 
	 * @param dstkey
	 * @param keys
	 *            可以使一个string 也可以是个string数组
	 * @return
	 */
	Long sinterstore(String dstkey, String... keys);

	/**
	 * <p>
	 * 通过key返回有set的并
	 * </p>
	 * 
	 * @param keys
	 *            可以使一个string 也可以是个string数组
	 * @return
	 */
	Set<String> sunion(String... keys);

	/**
	 * <p>
	 * 通过key返回有set的并,并存入到新的set
	 * </p>
	 * 
	 * @param dstkey
	 * @param keys
	 *            可以使一个string 也可以是个string数组
	 * @return
	 */
	Long sunionstore(String dstkey, String... keys);

	/**
	 * <p>
	 * 通过key将set中的value移除并添加到第二个set
	 * </p>
	 * 
	 * @param srckey
	 *            要移除的
	 * @param dstkey
	 *            添加
	 * @param member
	 *            set中的value
	 * @return
	 */
	Long smove(String srckey, String dstkey, String member);

	/**
	 * <p>
	 * 通过key获取set中value的值
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	Long scard(String key);

	/**
	 * <p>
	 * 通过key判断value是否是set中的元素
	 * </p>
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Boolean sismember(String key, String member);

	/**
	 * <p>
	 * 通过key获取set中随机的value,不删除元
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	String srandmember(String key);

	/**
	 * <p>
	 * 通过key获取set中所有的value
	 * </p>
	 * 
	 * @param key
	 * @return
	 */
	Set<String> smembers(String key);

	/**
	 * 通过key向zset中添加value,score,其中score就是用来排序 如果该value已经存在则根据score更新元素
	 * 
	 * @param key
	 * @param scoreMembers
	 * @return
	 */
	Long zadd(String key, Map<String, Double> scoreMembers);

	/**
	 * 通过key向zset中添加value,score,其中score就是用来排序 如果该value已经存在则根据score更新元素
	 * 
	 * @param key
	 * @param score
	 * @param member
	 * @return
	 */
	Long zadd(String key, double score, String member);

	/**
	 * 通过key删除在zset中指定的value
	 * 
	 * @param key
	 * @param members
	 *            可以使一个string 也可以是个string数组
	 * @return
	 */
	Long zrem(String key, String... members);

	/**
	 * 通过key增加该zset中value的score的?
	 * 
	 * @param key
	 * @param score
	 * @param member
	 * @return
	 */
	Double zincrby(String key, double score, String member);
	/**
	 * 为哈希表 key 中的域 field 的值加上增量 value
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	long hincrBy(String key, String field, long value);

	/**
	 * 通过key返回zset中value的排 下标从小到大排序
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Long zrank(String key, String member);

	/**
	 * 通过key返回zset中value的排 下标从大到小排序
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Long zrevrank(String key, String member);

	/**
	 * 通过key将获取score从start到end中zset的value socre从大到小排序 当start0 end-1时返回全
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Set<String> zrevrange(String key, long start, long end);

	/**
	 * 通过key返回指定score内zset中的value
	 * 
	 * @param key
	 * @param max
	 * @param min
	 * @return
	 */
	Set<String> zrangebyscore(String key, String max, String min);

	/**
	 * 通过key返回指定score内zset中的value
	 * 
	 * @param key
	 * @param max
	 * @param min
	 * @return
	 */
	Set<String> zrangeByScore(String key, double max, double min);

	/**
	 * 返回指定区间内zset中value的数
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @return
	 */
	Long zcount(String key, String min, String max);

	/**
	 * 通过key返回zset中的value个数
	 * 
	 * @param key
	 * @return
	 */
	Long zcard(String key);

	/**
	 * 通过key获取zset中value的score
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	Double zscore(String key, String member);

	/**
	 * 通过key删除给定区间内的元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Long zremrangeByRank(String key, long start, long end);

	/**
	 * 通过key删除指定score内的元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	Long zremrangeByScore(String key, double start, double end);

	/**
	 * 返回满足pattern表达式的有key keys(*) 返回有的key
	 * 
	 * @param pattern
	 * @return
	 */
	Set<String> keys(String pattern);

	/**
	 * 通过key判断值得类型
	 * 
	 * @param key
	 * @return
	 */
	String type(String key);


}
