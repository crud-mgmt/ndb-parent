package com.ndb.db.redis.cacheManager;

import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleCacheManager;

import com.ndb.db.redis.cacheManager.cache.StringCache;
import com.ndb.db.redis.convert.Converter;
import com.ndb.db.redis.factory.JedisFactory;

public class StringCacheManager extends AbstractRedisCacheManager {
	private JedisFactory factory;
	private String nameSpace;
	@Override
	protected Cache getMissingCache(String name) {
		return new StringCache(nameSpace, name, factory,getTimeout());
	}
	public JedisFactory getFactory() {
		return factory;
	}
	public void setFactory(JedisFactory factory) {
		this.factory = factory;
	}
	public String getNameSpace() {
		return nameSpace;
	}
	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}
	@Override
	public StringCache getCache(String name, String exprieType) {
		Integer timeout = (Integer)converter.convert(exprieType);
		this.timeout=timeout;
		return (StringCache) getCache(name);
	}

}
