package com.ndb.db.redis.factory;

import redis.clients.jedis.Jedis;

public abstract class AbstractJedisFactory implements JedisFactory {
	@Override
	public void returnResource(Jedis jedis) {
		if (jedis != null) {
			jedis.close();
		}
	}
}
