package com.ndb.db.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

public class SpringRedisTemplate {
	@Autowired
	private RedisTemplate<String,String> tmplate;
	
	public void hash() {
		tmplate.opsForValue();
	}
}
