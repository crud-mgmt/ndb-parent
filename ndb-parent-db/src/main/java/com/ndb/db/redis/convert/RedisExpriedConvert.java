package com.ndb.db.redis.convert;

import java.util.Map;

public class RedisExpriedConvert implements Converter {

	private Map<String,Integer> exprieMapping;
	
	private Integer defaultValue=Integer.valueOf(60);
	
	public void setExprieMapping(Map<String, Integer> exprieMapping) {
		this.exprieMapping = exprieMapping;
	}

	@Override
	public Integer convert(String key) {
		Integer val = exprieMapping.get(key);
		val=val==null?defaultValue:val;
		return val;
	}

}
