package com.ndb.parent.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.ndb.parent.common.exception.NdbException;
import com.ndb.parent.common.model.Req;



public class InvokeUtils {
	public static Object getInstance(String classId,Map<String,Object> data) {
		try {
			Class<?> reqClass = ClassLoader.getSystemClassLoader().loadClass(classId);
			return getInstance(reqClass,data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NdbException("");
		}
	}
	/***
	 * 如果没有子类自动加载父类接口
	 * @param classId
	 * @param data
	 * @return
	 */
	public static Object getReqInstance(String classId,Map<String,Object> data) {
		try {
			Class<?> reqClass = ClassLoader.getSystemClassLoader().loadClass(classId);
			return getInstance(reqClass,data);
		} catch (Exception e) {
			Class<?> reqClass=Req.class;
			return getInstance(reqClass,data);
		}
	}
	public static <T> T getInstance(Class<T> reqClass,Map<String,Object> data) {
		try {
			Field[] fields = reqClass.getDeclaredFields();
			List<Field> fieldList=new ArrayList<Field>(Arrays.asList(fields));
			getParentFields(reqClass,fieldList);
			T newInstance = reqClass.newInstance();
			if(CollectionUtils.isEmpty(data)) {
				return newInstance;
			}
			for (Field field : fieldList) {
				String fieldName=field.getName();
				if(StringUtils.equals(fieldName, "serialVersionUID")) {
					continue;
				}
				Object value = data.get(fieldName);
				if(value instanceof Map) {
					Map map=(Map) value;
					Class<?> cls=field.getType();
					value=getInstance(cls, map);
				}else if(value instanceof List) {
					List<Object> list=new ArrayList<Object>();
					Type listCls=field.getGenericType();
					if(listCls!=null && listCls instanceof ParameterizedType) {
						ParameterizedType paramType=(ParameterizedType) listCls;
						Class<?> paramObj = (Class<?>) paramType.getActualTypeArguments()[0];
						List<?>val=(ArrayList)value;
						for (Object obj : val) {
							if(obj instanceof Map) {
								Object tmpVal = getInstance(paramObj, (Map)obj);
								list.add(tmpVal);
							}else {
								list.add(obj);
							}
						}
					}
					value=list;
				}
				String methodName="set".concat(upperName(fieldName));
				Method fmethod = reqClass.getMethod(methodName, field.getType());
				fmethod.invoke(newInstance, value);
			}
			return newInstance;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NdbException("");
		}
	}
	private static void getParentFields(Class<?> reqClass, List<Field> fieldList) {
		Class<?> currClass=reqClass.getSuperclass();
		while (currClass != null){
		    fieldList.addAll(new ArrayList<>(Arrays.asList(currClass.getDeclaredFields())));
		    currClass = currClass.getSuperclass();
		  }
	}

	public static String upperName(String name) {
		char[] cs = name.toCharArray();
		cs[0] -= 32;
		return String.valueOf(cs);
	}
}
