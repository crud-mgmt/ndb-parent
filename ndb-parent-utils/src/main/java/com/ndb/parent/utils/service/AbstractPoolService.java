package com.ndb.parent.utils.service;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * 采用多线程处理数据
 * @author Administrator
 *
 * @param <T>
 */
public abstract class AbstractPoolService<T> {
	private static int POO_SIZE=8;
	private ExecutorService pool = Executors.newFixedThreadPool(POO_SIZE);
	public void action(List<T> list) {
		ConcurrentLinkedDeque<T> queue = new ConcurrentLinkedDeque<T>();
		queue.addAll(list);
		for (int i = 0; i < POO_SIZE; i++) {
			pool.execute(new Runnable() {
				@Override
				public void run() {
					T item = queue.pollFirst();
					while(item!=null) {
						doAction(item);
						item = queue.pollFirst();
					}
				}
			});
		}
	}
	
	public abstract void doAction(T item);
	
}
