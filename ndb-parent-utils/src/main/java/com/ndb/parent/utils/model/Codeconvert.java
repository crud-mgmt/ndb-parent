package com.ndb.parent.utils.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
@Configuration
public class Codeconvert {
	private Map<String,String> code=new HashMap<String, String>();
	
	public Codeconvert() {
		load();
	}

	public void load() {
		try {
			File file = ResourceUtils.getFile("classpath:Msg.properties");
			Properties prop=new Properties();
			prop.load(new FileInputStream(file));
			 Iterator<String> it=prop.stringPropertyNames().iterator();
              while(it.hasNext()){
                  String key=it.next();
                  code.put(key, prop.getProperty(key));
             }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getCode(String key) {
		String msg = code.get(key);
		if(StringUtils.isEmpty(msg)) {
			msg="未知错误";
		}
		return msg;
	}
	
	
	@Bean
	public Codeconvert codeconvert() {
		return new Codeconvert();
	}
}
