package com.ndb.parent.utils.config;

import org.apache.dubbo.rpc.AsyncRpcResult;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.csp.sentinel.adapter.dubbo.config.DubboAdapterGlobalConfig;
import com.alibaba.csp.sentinel.adapter.dubbo.fallback.DubboFallback;
import com.alibaba.csp.sentinel.slots.block.BlockException;

@Configuration
public class ServiceDownConfig implements InitializingBean{

	@Override
	public void afterPropertiesSet() throws Exception {
		/**
		 * 服务降级，超过流控规则时返回异常
		 */
		DubboAdapterGlobalConfig.setProviderFallback(new DubboFallback() {
			@Override
			public Result handle(Invoker<?> invoker, Invocation invocation, BlockException ex) {
				return AsyncRpcResult.newDefaultAsyncResult("服务器处理不过来啦", invocation);
			}
		});
	}

}
